# 2. Post-processing hmmscan  --------------------------------------------------

# 2.1 Unifies hmmscan files  ---------------------------------------------------

union_hmm <- 
  function(dir){
    
    files = dir(dir)
    files = files[grep("hmmscan",x = files)]
    ind = sapply(X = strsplit(x = files,split = "_"),
                 FUN = function(x){ x[1] },
                 simplify = TRUE)
    files = files[order(ind)]
    files = paste(dir,files,sep = "")
    OUT = paste(dir,"HMMscan.txt",sep = "")
    cmd = paste("cat",paste(files,collapse=" "),
                ">",OUT,collapse="")
    system(cmd,wait=TRUE)
    
    return(OUT)
    
  }

# 2.2 Read hmmscan  ------------------------------------------------------------

hmmscan2matrix <-
  function(file){
    
    command = "sed '/^\\#/d'" 
    cmd = paste(command,file)
    lines = system(cmd,intern=TRUE)
    
    parsingLine <-
      function(x){
        row_x = unlist(strsplit(x,split="  *",perl=TRUE))
        long_x_ = length(row_x)
        row_x = c(row_x[1:22],paste(row_x[23:long_x_],collapse=" "))
        return(row_x)
      }
    
    file_rows = lapply(X = lines,parsingLine)
    tbl_hmm = do.call(rbind,file_rows)
    
    cnames <- c("target name","accession","tlen","query name","-","qlen",
                "E-value","score-s","bias-s","#","of","c-Evalue",
                "i-Evalue","score-d","bias-d","hmm begin","hmm end",
                "ali begin","ali end","env begin","env end","acc",
                "description of target")
    
    dimnames(tbl_hmm) <- list(NULL,cnames)
    return(tbl_hmm)
  }

as.hmmscan <-
  function(x,...){
    
    l = list(...)
    tbl = l$tbl
    index = which( x == tbl[,"query name"] ) 
    
    target_name =             tbl[index,"target name"]
    accession   =             tbl[index,"accession"]
    tlen        = as.numeric( tbl[index,"tlen"]       )
    qlen        = as.numeric( tbl[index,"qlen"]       )
    E_value     = as.numeric( tbl[index,"E-value"]    )
    score_s     = as.numeric( tbl[index,"score-s"]    )
    bias_s      = as.numeric( tbl[index,"bias-s"]     )
    n           = as.numeric( tbl[index,"#"]          )
    of          = as.numeric( tbl[index,"of"]         )
    c_Evalue    = as.numeric( tbl[index,"c-Evalue"]   )
    i_Evalue    = as.numeric( tbl[index,"i-Evalue"]   )
    score_d     = as.numeric( tbl[index,"score-d"]    )
    bias_d      = as.numeric( tbl[index,"bias-d"]     )
    hmm_begin   = as.numeric( tbl[index,"hmm begin"]  )  
    hmm_end     = as.numeric( tbl[index,"hmm end"]    )
    ali_begin   = as.numeric( tbl[index,"ali begin"]  )  
    ali_end     = as.numeric( tbl[index,"ali end"]    )
    env_begin   = as.numeric( tbl[index,"env begin"]  ) 
    env_end     = as.numeric( tbl[index,"env end"]    )
    acc         = as.numeric( tbl[index,"acc"]        )
    description =             tbl[index,"description of target"]
    
    hmmscan = list( target_name = target_name,
                    accession   = accession,
                    tlen        = tlen,
                    qlen        = qlen,
                    E_value     = E_value,
                    score_s     = score_s,
                    bias_s      = bias_s,
                    n           = n,
                    of          = of,
                    c_Evalue    = c_Evalue,
                    i_Evalue    = i_Evalue,
                    score_d     = score_d,
                    bias_d      = bias_d,
                    hmm_begin   = hmm_begin,
                    hmm_end     = hmm_end,
                    ali_begin   = ali_begin,
                    ali_end     = ali_end,
                    env_begin   = env_begin,
                    env_end     = env_end,
                    acc         = acc,
                    description = description
    )
    print(x)
    return(hmmscan)
  }

hmmscan2R <-
  function(file,n_cores=1){
    
    tbl = hmmscan2matrix(file)
    query_name = levels(as.factor(tbl[,"query name"]))
    hmmscan = mclapply(query_name,as.hmmscan,tbl=tbl,mc.cores=n_cores)
    names(hmmscan) <- query_name
    
    return(hmmscan)
  }


# 2.3 Sort hmmscan  ------------------------------------------------------------

order.hmmscan.field <-
  function(x,...){
    
    l = list(...)
    field = l$field
    
    ind_order = order(x[[field]])
    
    return(ind_order)
  }

sort.hmmscan.field <-
  function(x,...){
    
    l = list(...)
    ind_order = l$ind_order
    
    return(x[ind_order])
  }

sort.hmmscan <-
  function(x,...){
    
    l = list(...)
    field = l$field
    
    ind_order = order.hmmscan.field(x,field=field)
    x = lapply(x,sort.hmmscan.field,ind_order=ind_order)
    
    return(x)
  }

sort.list.hmmscan <-
  function(x,field,n_cores=2){
    
    x = mclapply(x,sort.hmmscan,field=field,mc.cores=n_cores)
    
    return(x)
  }

# 2.4 Overlaping post-procesing  -----------------------------------------------


overlap.internal <-
  function(B1,B2,E1,E2,type = "residues"){
    
    AL1 = E1-B1
    AL2 = E2-B2
    
    if( B1 > B2 ){
      
      k1 = B2
      k2 = E2
      B2 = B1
      E2 = E1
      B1 = k1
      E1 = k2
      
    }
    
    L1 = E1-B1
    L2 = E2-B2
    
    if( B1 == B2 ){
      
      LO = ifelse( E1 > E2,
                   yes = L2,
                   no = L1 )
      
    } else{ 
      
      if( B2 > E1 ){
        
        LO = 0
        
      } else{ 
        
        if( B2 < E1 ){
          
          LO = ifelse( (E1-B2) > L2,
                       yes = L2,
                       no = E1-B2 )
          
        } else{
          
          LO = 0
          
        } 
      } 
    } 
    
    switch(type,
           residues = LO,
           percentage = ( LO/( (L1+L2)/2 ) )*100,
           percentage_1 = ( LO/AL1 )*100,
           percentage_2 = ( LO/AL2 )*100)
    
  }

# This function joint the families with a overlaping percentage higher than 10%
# the overlaping percentage is calculate according to the equation (1)

m_overlap.protein <-
  function(x){
    
    Families = x[["target_name"]]
    BE = rbind(x[["env_begin"]],x[["env_end"]])
    E_value = matrix(x[["i_Evalue"]],nrow = 1)
    
    trik = as.character( seq_len( length(Families) ) )
    nombres = Families
    
    colnames(E_value) <- trik
    colnames(BE) <- trik
    
    if( length(Families) !=1 ){
      
      i = 0 ; j = 0
      
      while( (ncol(BE)+1) > (i+2) ){
        
        OBE = BE[c(1,2),c(i+1,i+2)]
        
        p1_overlap = overlap.internal(B1 = OBE[1,1],
                                      B2 = OBE[1,2],
                                      E1 = OBE[2,1],
                                      E2 = OBE[2,2],
                                      type = "percentage_1") 
        
        p2_overlap = overlap.internal(B1 = OBE[1,1],
                                      B2 = OBE[1,2],
                                      E1 = OBE[2,1],
                                      E2 = OBE[2,2],
                                      type = "percentage_2") 
        
        rule = ( p1_overlap > 10 ) && ( p2_overlap > 10 )
        
        if( rule ){
          
          NB = min(OBE)
          NE = max(OBE)
          NBE = matrix(c(NB,NE),ncol = 1)
          ind = colnames(OBE)
          
          if( E_value[1,ind[2]] > E_value[1,ind[1]] ){
            
            colnames(NBE) <- ind[1]
            
          } else{
            
            colnames(NBE) <- ind[2]
            
          }
          
        } else{
          
          NBE = OBE
          j = 1
          
        }
        
        if( (i+3)>ncol(BE) ){
          
          k = integer(0)
          
        }else{
          
          k = seq(i+3,ncol(BE))
          
        }
        
        BBE = BE[,seq_len(i),drop=FALSE]
        ABE = BE[,k,drop=FALSE]
        BE = cbind(BBE,NBE,ABE)
        i = i + j ; j = 0
        
      }
      
    } else{
      
      colnames(BE) = trik
      
    }
    
    row.names(BE) <- c("B","E")
    
    selE_values <-
      function(x,E_value){
        
        ind = which( x == colnames(E_value) ) 
        Evaule = min( E_value[1,ind] )
        
        return(Evaule)
      }
    
    E_values = sapply(X = colnames(BE),                                         # update
                      FUN = selE_values,
                      E_value = E_value,
                      simplify = "matrix",
                      USE.NAMES = TRUE)
    
    
    colnames(BE) <- nombres[as.numeric( colnames(BE) )]
    names(E_values) <- colnames(BE)                                             # update
    
    protein = list()                                                            # update
    
    protein[["E_value"]] = E_values                                             # update
    protein[["m_overlap"]] = BE
    
    return(protein)
    
  }

m_overlap.proteins <-
  function(x,n_cores = 1){
    
    proteins = mclapply(X = x,
                        FUN = m_overlap.protein,
                        mc.cores = n_cores)
    
    return(proteins)
    
  }

# 2.5 Clans  -------------------------------------------------------------------

# x: accessed object
# access: I want to access
# key: through which I want to access
# type: access type, this only accepts an object of type logical
#
# EXAMPLE pfam_access(x=pfamA,key="repeat?",access="pfamA_id",type=TRUE) 

pfam_access1 <-
  function(x,key,access,type=TRUE){
    x[which(x[,key]==type),access]
  }

is.allowed <-    
  function(a,b){
    
    int = which(a==b)
    is.integer(int) && length(int) == 0
  }

repeat.domain <-
  function(x,...){
    
    l = list(...)
    repeat.pfamA = l$repeat.pfamA
    
    !(is.allowed(repeat.pfamA,x))
  }

pfam_access <-
  function(x,y,key,access){
    
    indices = lapply(X = y,
                     FUN = function(x,ckey){
                             y = which(x == ckey)
                             return(y)
                           },
                     ckey = x[,key])
    
    i = which( colnames(x) == access )
    x = rbind( x, paste("N_",access,sep="") )
    ind_sub = which( rapply(indices,length) == 0 )
    indices[ind_sub] = nrow(x)
    indices = unlist(indices)
    data = x[indices,access]
    
    return(data)
  }

clans.protein <-
  function(x,...){
    
    l = list(...)
    repeat.pfamA = l$repeat.pfamA
    clan_membership = l$clan_membership
    
    families = colnames(x[["m_overlap"]])
    Repeat = sapply(families,repeat.domain,
                    repeat.pfamA = repeat.pfamA)
    
    Clan = pfam_access(x = clan_membership,y = families,
                       key = "pfamA_id",access = "clan_id")
    
    Clans = rbind(Repeat,Clan)
    colnames(Clans) <- families
    x$Clans = Clans
    
    return(x)
  }

clans.proteins <-
  function(x,clan_membership,repeat.pfamA,n_cores=1){
    
    proteins = mclapply(x,clans.protein,clan_membership=clan_membership,
                        repeat.pfamA=repeat.pfamA,n_cores=n_cores)  
    
    return(proteins)
  }

# REQUIRE pfamA_repeat
#         pfamA_repeat = pfam_access(x=pfamA,key="repeat?",access="pfamA_id",type=TRUE)   # The pfamA_id with "repeat"
#
# hmmscan = clans.proteins(x=hmm_overlap,repeat.pfamA=pfamA_repeat)


# 2.6 Repeat post-procesing  ---------------------------------------------------

rule <-
  function(x,y){
    
    I = rep(FALSE,3)
    
    families = c( colnames(x) , colnames(y) )
    repeats = c( as.logical(x["Repeat",1]) , as.logical(y["Repeat",1]) )
    clans = c( x["Clan",1] , y["Clan",1] )
    
    if( families[1] == families[2] ){ I[1] <- TRUE }
    if( (repeats[1] == repeats[2])&&(repeats[1] == TRUE) ){ I[2] <- TRUE }
    if( (clans[1] == clans[2])&&(clans[1] != "N_clan_id") ){ I[3] <- TRUE }
    
    rule = ( I[1] && I[2] ) || ( I[2] && I[3] )
    
    return(rule)
  }

rename <-
  function(x,clans){
    
    new_names = sapply(X = x,
                       FUN = function(x,clans){
                         
                         if( length(x) == 1 ){                                  # only family
                           
                           new_name = x
                           
                         } else{
                           
                           x = base::unique(x)
                           
                           if( length(x) == 1 ){                                # families repeats
                             
                             new_name = x
                             
                           } else{                                              # clans repeats
                             
                             new_name = clans["Clan",x[1]]
                             
                           }
                           
                         }
                         
                         return(new_name)
                         
                       },
                       clans = clans,
                       simplify = TRUE,
                       USE.NAMES = FALSE
    )
    
    return(new_names)
    
  }

create.connexion <-
  function(x){
    
    nx = length(x)
    connexion = matrix(data = as.logical(diag(nx)),
                       ncol = nx,
                       dimnames = list(x,x))
    
    return(connexion)
    
  }

repeat.protein <-
  function(x){
    
    BE = x[["m_overlap"]]
    E_value = x[["E_value"]]
    Clans = x[["Clans"]]
    
    families = colnames(BE) 
    members = list()
    h = character()
    
    i = 0 
    j = 0 
    l = 0
    
    if( ncol(BE) != 1 ){ 
      
      while( (ncol(BE)+1) > (i+2) ){
        
        l = l + 1
        h = c(h,families[l])
        members[[i+1]] = h
        
        OBE = BE[,c(i+1,i+2)]
        names = colnames(OBE)
        
        decision = rule(x = Clans[,names[1],drop = FALSE],
                        y = Clans[,names[2],drop = FALSE])  
        
        if( decision ){
          
          NB = min(OBE)
          NE = max(OBE)
          NBE = matrix(c(NB,NE),ncol = 1)
          
          colnames(NBE) <- ifelse( E_value[names[1]] < E_value[names[2]], 
                                   yes = names[1], 
                                   no = names[2] )
          
        } else{
          
          NBE = OBE
          j = 1
          
        }
        
        if( ( i + 3 ) > ncol( BE ) ){
          
          k = integer(0)
          
        } else{
          
          k = seq( i + 3 ,ncol( BE ))
          
        }
        
        BBE = BE[,seq_len(i),drop = FALSE]
        ABE = BE[,k,drop = FALSE]
        BE = cbind(BBE,NBE,ABE)
        
        i = i + j
        
        if( j != 0){
          
          h = character() 
          
        }
        
        j = 0
        
      }
      
    } else{
      
      names(families) <- NULL
      
    } 
    
    l = l + 1
    h <- c(h,families[l])
    members[[i+1]] <- h
    
    new_names <- rename(members,Clans)
    names(members) <- new_names
    colnames(BE) <- new_names
    rownames(BE) <- c("Begin","End")
    
    connexion <- create.connexion(new_names)
    
    x <- list()
    x[["Begin_End"]] <- BE
    x[["Connexion"]] <- connexion
    x[["Members"]] <- members
    
    return(x)
    
  }

repeat.proteins <-
  function(x,n_cores = 1){
    
    proteins = mclapply(X = x,
                        FUN = repeat.protein,
                        mc.cores = n_cores)
    
    return(proteins)
  }


# 2.7 The families id are rename  ----------------------------------------------

# The families id are rename by distinguish 

# x = character 
# y = numeric
# assigned = character vector 

assignment <-
  function(x,y,assigned,sep="."){
    
    z = paste(x,y,sep=sep)
    
    if( z%in%assigned ){ 
      
      assignment(x,y+1,assigned,sep=sep)
      
    } else{
      
      return(z)
      
    }
    
  }

# x = character vector 

distinguish <-
  function(x){
    
    assigned = character(0)
    
    for(i in x){
      
      temp = assignment(i,1,assigned,sep="|")
      assigned = c(assigned,temp)
      
    }
    
    return(assigned)
    
  }

distinguish_interface <-
  function(x){
    
    Begin_End <- x[["Begin_End"]]
    Connexion <- x[["Connexion"]]
    
    colnames(Begin_End) <- distinguish(colnames(Begin_End))
    colnames(Connexion) <- distinguish(colnames(Connexion))
    rownames(Connexion) <- distinguish(rownames(Connexion))
    
    x[["Begin_End"]] <- Begin_End 
    x[["Connexion"]] <- Connexion
    
    return(x)
    
  }

distinguish.chains <-
  function(x){
    
    x = sapply(X = x,
               FUN = distinguish_interface,
               simplify = FALSE,
               USE.NAMES = TRUE)
    
    return(x)
    
  }


# 2.8 Mapping of SEQRES position to ATOM position  -----------------------------

# The definition are mapping of residues position to atom position 

# Seq = Sequence obtained from pdb
# ResPos = Residues position
# AtomPos = Atom position

ResPos2Def <-
  function(ResPos,Seq){
    
    nResPos = ncol(Seq)
    RangeDef = c(min( which( colnames(Seq) != "-" ) ),
                 max( which( colnames(Seq) != "-" ) ) )
    
    Def = colnames( Seq[,ResPos,drop=FALSE] )
    k = 1
    
    while( Def == "-" ){
      
      ResPos = ResPos + ceiling(k/2)*((-1)^k)
      
      if( ResPos <= 1 ){
        
        ResPos = RangeDef[1]
        
      }
      
      if( ResPos >= nResPos ){
        
        ResPos = RangeDef[2]
        
      }
      
      Def = colnames( Seq[,ResPos,drop=FALSE] )
      k = k + 1
      
    }
    
    return(Def)
    
  }

Def2AtomPos <-
  function(Def,Seq){
    
    Seq = Seq[ ,(colnames(Seq) != "-" ),drop = FALSE]
    AtomPos = which( colnames(Seq) == Def )[1]                                  # [1] "1yrq_B", "2ku2_G"
    
    return(AtomPos)
    
  }

ResPos2AtomPos <-
  function(ResPos,Seq){
    
    Def = ResPos2Def(ResPos,Seq)
    AtomPos = Def2AtomPos(Def,Seq)
    
    return(AtomPos)
    
  }

ResPos2AtomPos.chain <-
  function(x,seq){
    
    BE_ResPos = x[["Begin_End"]]
    BE_AtomPos = sapply(X = as.vector(BE_ResPos),
                        FUN = ResPos2AtomPos,
                        Seq = seq,
                        simplify = TRUE,
                        USE.NAMES = FALSE)
    BE_AtomPos = matrix(data = BE_AtomPos,
                        nrow = nrow(BE_ResPos),
                        ncol = ncol(BE_ResPos),
                        dimnames = dimnames(BE_ResPos))
    
    x[["Begin_End"]] <- BE_AtomPos
    
    return(x)
    
  }

ResPos2AtomPos.chains <-
  function(x,seq){
    
    chain = names(x)
    definition = mapply(FUN = ResPos2AtomPos.chain,
                        x = x[chain],
                        seq = seq[chain],
                        SIMPLIFY = FALSE,
                        USE.NAMES = TRUE)
    
    return(definition)
  }


# 2.9 Families identified only in sequence are removed  ------------------------

# x: Pfam_definition

delsfam.chain <-
  function(x){
    
    BE = x[["Begin_End"]]
    Con = x[["Connexion"]]
    Mem = x[["Members"]]
    
    diff = BE["End",] - BE["Begin",]
    
    ind = which( diff < 15 )
    
    if( ncol(BE) != length(ind) ){
      
      select = setdiff(colnames(BE),names(ind))
      
      NBE = BE[,select,drop = FALSE]
      NCon = Con[select,select,drop = FALSE]
      
      select = strsplit(x = select,split = "\\|")
      
      select = sapply(X = select,
                      FUN = function(x){ 
                        return(x[1]) 
                      },
                      simplify = TRUE,
                      USE.NAMES = FALSE)
      
      NMem = Mem[select]
      
      x[["Begin_End"]] = NBE
      x[["Connexion"]] = NCon
      x[["Members"]] = NMem
      
    } else{
      
      x = NULL
      
    }
    
    return(x)
    
  }

delsfam.chains <-
  function(x){
    
    x = sapply(X = x,
               FUN = delsfam.chain,
               simplify = FALSE,
               USE.NAMES = TRUE)
    
    ind = sapply(X = x,
                 FUN = is.null)
    
    empty = names(which(ind))
    x[empty] = NULL
    
    return(x)
    
  }
