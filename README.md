# Profs Package for relate Domain with Proteins Families #

Uses the definitions given by CATH domains for expands the information obtained through profiles of Pfam database.

## Features ##

The pipeline has capacity to relate the structural information of CATH with sequence information of Pfam, construction of libraries by families or clans. If the information is not available CATH, a alignment process is developed. In Addition, fragmented, repeat domains and domains formed by several families (supradomain) are supported. Please refer to the main [wiki Profs](https://bitbucket.org/afgiraldofo/profs/wiki/Home) for more background information.


## Requirements ##

* Software
    * [R v3.0.3](http://cran.utstat.utoronto.ca/)
    * [muscle v3.8.31]( http://www.drive5.com/muscle/downloads3.8.31/muscle3.8.31_i86linux64.tar.gz)
    * [cd-hit v4.5.4](https://cdhit.googlecode.com/files/cd-hit-v4.5.4-2011-03-07.tgz)
    * [hmmscan & hmmpress v3.0](http://selab.janelia.org/software/hmmer3/3.0/hmmer-3.0-linux-intel-x86_64.tar.gz)
    * [blastp & makeblastdb v2.2.29+](ftp://ftp.ncbi.nih.gov/blast/executables/LATEST/ncbi-blast-2.2.29+-x64-linux.tar.gz)

* R packages
    * [bio3d 2.0-1](http://thegrantlab.org/bio3d/download/download-bucket?download=1:bio3d-v2-0-1  )
    * [seqinr 3.0-7](http://cran.r-project.org/src/contrib/seqinr_3.0-7.tar.gz)
    * [Biostrings 2.30-1](http://www.bioconductor.org/packages/2.14/bioc/html/Biostrings.html)

* Database
    * [PDB](ftp://ftp.wwpdb.org/pub/pdb/data/structures/all/pdb/)
    * [CATH v4.0.0](http://download.cathdb.info/cath/releases/all-releases/v4_0_0/cath-classification-data/cath-domain-boundaries-v4_0_0.txt)
    * [Pfam v30.0](ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/)
        * [Pfam-A.hmm.gz](ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz)
        * [clan_membership.txt](ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/clan_membership.txt.gz) 
        * [clan.txt](ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/clan.txt.gz)
        * [pfamA.txt](ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/pfamA.txt.gz)


## Instructions ##

1. Clone the Profs repository: `$ git clone  https://bitbucket.org/afgiraldofo/profs.git`.
2. Build the Profs R package: `$ R CMD build profs`.
3. Install Profs R package: `$ sudo R CMD INSTALL ./Profs_0.99.tar.gz --no-lock --clean`. If package installed successfully,  go to the next step.
4. Load the Profs package from inside the R terminal by typing `> require("Profs")`. To find the location of external binaries type: `> system.file("bin",package = "Profs")`.
5. Add the location of external binaries to the system path variable. This can be done by adding the `export PATH="path:$PATH";` line to the "~/.bashrc" file, where `path` is the path to external binaries found in step 4.

*NOTE:* The R version was testing with the 3.10 version, 2.14.1 or lower have compatibility problems.